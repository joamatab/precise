function func = prbs( BitOrder, Period, Delay, RestValue, Range )
%PRBS Returns the time interpolator of a PRBS signal.
%   PRBS( BitOrder, Period ) returns the time interpolator of a PRBS signal
%       of order 'BitOrder' and period 'Period'.
%   PRBS( _, _, Delay, RestValue) pads the PRBS signal in front with
%       'RestValue' for a time of 'Delay' [ps]. The same level is kept also
%       after the completion of the signal. If 'RestValue' is empty [], the
%       prbs signal is repeated indefinitely.
%   PRBS( _, _, _, _, Range) sets the low bit-level to 'min(Range)' and the
%       high bit-level to 'max(Range)'.
%
%   Arguments:
%       - BitOrder (required): order of pseudorandom bit sequence.
%       - Period (required): period of each bit in [ps].
%       - Delay (default: 0): delay of the sequence in [ps].
%       - RestValue (default: 0): value of the signal at rest, i.e. during
%           the delay and after the sequence is terminated. If 'RestValue'
%           is empty [], the prbs signal is repeated indefinitely.
%       - Range (default: [0, 1]): low and high level of the bit sequence.
%           The values of the low and high levels are the minimum and the
%           maximum values of the array respectively.
%
%   Note: this function relies on the `prbs` function of the SerDes
%       Toolbox and Matlab(R) 2020b for `mustBeScalarOrEmpty`
    arguments
        BitOrder (1,1) double { mustBePositive, mustBeInteger }
        Period (1,1) double { mustBePositive }
        Delay (1,1) double { mustBeNonnegative } = 0
        RestValue double { mustBeScalarOrEmpty, mustBeNonNan } = 0
        Range (1,2) double { mustBeFinite } = [0, 1]
    end

    % generate PRBS
    data = prbs( BitOrder, 2^BitOrder )';
    if isempty(RestValue)
        % add point to create 2^BitOrder sections
        data = [data; data(1)];
    else
        % complete combination with first bits
        data = [data; data(1:BitOrder-1); RestValue];
    end
    % rescale data
    data = rescale( data, Range(1), Range(2) );
    
    N = length(data)-1;
    
    if isempty(RestValue)
        func = @(t) interp1( Period*(0:N), data, mod( t-Delay, Period*N ), ...
            'previous' );
    else
        func = @(t) interp1( Delay + Period*(0:N), data, t, ...
            'previous', RestValue);
    end
end
