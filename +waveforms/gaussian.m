function func = gaussian( FWHM, Center, Normalize )
%GAUSSIAN Returns the time interpolator of a PRBS signal.
%   GAUSSIAN( FWHM ).
%   GAUSSIAN( _, Center ).
%   GAUSSIAN( _, _, Normalize ).
%
%   Arguments:
%       - FWHM (required): Full-Width Half-Maximum in [ps].
%       - Center (default: 0): Center of the Gaussian in [ps].
%       - Normalize (default: 'Peak'): normalization of the gaussian,
%           either 'Peak' or 'Area'. In 'Peak' the value for 't = Center'
%           is 1, while in 'Area' the total area under the gaussian is
%           equal to 1.
    arguments
        FWHM (1,1) double { mustBePositive, mustBeFinite }
        Center (1,1) double { mustBeFinite } = 0
        Normalize { mustBeMember( Normalize, {'Peak', 'Area'} ) } = 'Peak'
    end

    % FWHM = 2*sqrt(2*log(2)) * Sigma;
    Sigma = FWHM / ( 2*sqrt(2*log(2)) );
    
    switch Normalize
        case 'Peak'
            func = @(t)   exp( -0.25 * ( ( t - Center ) / Sigma ).^2 );
        case 'Area'
            N = sqrt( 1/(Sigma*sqrt(2*pi)) );
            func = @(t) N*exp( -0.25 * ( ( t - Center ) / Sigma ).^2 );
    end
end
