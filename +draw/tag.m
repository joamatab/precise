function T = tag( ax, x, y, string, BackgroundColor, EdgeColor )
%TAG Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x
        y 
        string (1,:) char
        BackgroundColor = 'w'
        EdgeColor = 'k'
    end
    T = text( ax, x, y, string, ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle', ...
        'BackgroundColor', BackgroundColor, ...
        'EdgeColor', EdgeColor, ...
        'Clipping', 'on');
end
