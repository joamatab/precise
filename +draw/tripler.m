function [Guides, Inputs, Outputs, Tags ] = tripler( ax, x, y, i, ...
    flipLR, flipUD, transpose )
%TRIPLER Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x (1,1) double
        y (1,1) double
        i (1,1) double { mustBeInteger }
        flipLR logical = false
        flipUD logical = false
        transpose logical = false
    end
    ports = i+[0 5; 1 4; 2 3];
    if flipLR
        ports = fliplr( ports );
    end
    if flipUD
        ports = flipud( ports );
    end
    if transpose
        ports = ports.';
    end
    Tags = precise.draw.port( ax, x, y, num2str( ports ) );
    Guides = [];
    Inputs = [];
    Outputs = [];
end
