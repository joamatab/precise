function [Guides, I, O, Tags ] = IOport( ax, x, y, color, args )
%IOPORT Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x (1,1) double
        y (1,1) double
        color (:,:) = ['y';'b']
        args.Length (1,1) { mustBePositive } = 0.03
        args.Size (1,1) { mustBePositive } = 0.05
        args.Direction { mustBeMember( args.Direction, ...
            ["left", "right", "up", "down" ] ) } = "right"
    end
    
    switch args.Direction
        case "right"
            a = 0;
        case "up"
            a = 90;
        case "left"
            a = 180;
        case "down"
            a = 270;
    end
    S = args.Size;
    L = args.Length;
    body = [
        0, -S/3, -S/3,  -S,   -S, -S/3, -S/3;
        0,    L,  L/3, L/3, -L/3, -L/3,   -L
    ];
    R = @(a) [ ...
        cosd(a) sind(a); ...
        -sind(a), cosd(a) ...
    ];
    IBody = [x;y] + R(a)*body;
    OBody = [x;y] + R(180+a)*body;

    % Set hold to true
    tf = ishold( ax );
    hold( ax, 'on' );

    I = patch( ax, IBody(1,:), IBody(2,:), color( 1, : ) );
    O = patch( ax, OBody(1,:), OBody(2,:), color( 2, : ) );

    % Reset hold to old position
    if tf
        hold( ax, 'on' );
    else
        hold( ax, 'off' );
    end
    Guides = [];
    Tags = [];
end


% function [Guides, I, O, Tags ] = IOport( ax, x, y, color, args )
% %IOPORT Summary of this function goes here
% %   Detailed explanation goes here
%     arguments
%         ax
%         x (1,1) double
%         y (1,1) double
%         color (:,:) = ['y';'b']
%         args.LineWidth (1,1) { mustBePositive } = 10
% %         args.Size (1,1) { mustBePositive } = 0.1
%         args.Direction { mustBeMember( args.Direction, ...
%             ["left", "right", "up", "down" ] ) } = "right"
%     end
%     
%     drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1), ...
%         'AutoScale','off');
% 
%     if any( strcmp( args.Direction, ["right", "left"] ) )
% %         x = x + args.Size .* [0, 0, 1; 0, 0, -1];
% %         y = y + args.Size .* [0.5, -0.5, 0; -0.5, 0.5, 0];
%     elseif any( strcmp( args.Direction, ["up", "down"] ) )
% %         x = x + args.Size .* [0.5, -0.5, 0; -0.5, 0.5, 0];
% %         y = y + args.Size .* [0, 0, 1; 0, 0, -1];
%     end
%     if any( strcmp( args.Direction, ["left", "down"] ) )
%         x = flip( x );
%         y = flip( y );
%     end
% 
%     % Set hold to true
%     tf = ishold( ax );
%     hold( ax, 'on' );
% 
%     I = patch( ax, x(1,:), y(1,:), color( 1, : ) );
%     O = patch( ax, x(2,:), y(2,:), color( 2, : ) );
% 
%     % Reset hold to old position
%     if tf
%         hold( ax, 'on' );
%     else
%         hold( ax, 'off' );
%     end
%     Guides = [];
%     Tags = [];
% end
