function T = port( ax, x, y, string )
%PORT Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x
        y 
        string
    end
    T = text( ax, x, y, string, ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle', ...
        'Clipping', 'on');
end
