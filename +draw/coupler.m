function [Guides, Inputs, Outputs, Tags ] = coupler( ax, x, y, i, ...
    flipLR, flipUD, transpose )
%COUPLER Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x (1,1) double
        y (1,1) double
        i (1,1) double { mustBeInteger }
        flipLR logical = false
        flipUD logical = false
        transpose logical = false
    end
    ports = i+[3 2; 0 1];
    if flipLR
        ports = fliplr( ports );
    end
    if flipUD
        ports = flipud( ports );
    end
    if transpose
        ports = ports.';
    end
    Tags = precise.draw.port( ax, x, y, num2str( ports ) );
    Guides = [];
    Inputs = [];
    Outputs = [];
end
