function [Guides, Inputs, Outputs, Tags ] = scissor( ax, x0, y0, p, ...
    g, N, closed, color, args )
%SINGLE_RING Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x0 (1,1) double
        y0 (1,1) double
        p (1,1) double { mustBeInteger }
        g (1,1) double { mustBeInteger }
        N (1,1) double { mustBeInteger, mustBePositive }
        closed logical
        color (:,:) = repmat( ('ymcrgbk')', ...
            ceil( (4*N - 2 + closed)/7 ), 1 )
        args.HSpace (1,1) double { mustBePositive } = 1;
        args.Width (1,1) double { mustBePositive } = 5
    end
    Tot = 4*N - 2 + closed;
    hs = args.HSpace;

    % Set hold to true
    tf = ishold( ax );
    hold( ax, 'on' );
    
    % Preallocate memory for graphics objects
    Guides = gobjects( Tot, 1 );

    gid = 1;
    for in = 1:N
        y = y0;
        x = x0 + (in-1)*hs;
        precise.draw.coupler( ax, x, y+0.15, p );
        y = y + 0.3;
        p = p + 4;

        [ Guides( gid + (0:1) ), ~, ~, ~ ] = precise.draw.circle( ax, ...
            x, y+0.2, g, 2, ...
            'Color', color( gid + (0:1) ), ...
            'Radius', 0.2, ...
            'Width', args.Width );
        y = y + 0.4;
        g = g + 2;
        gid = gid + 2;

        precise.draw.coupler( ax, x, y+0.15, p );
        y = y + 0.3;
        p = p + 4;
    end
    for in = 1:N-1
        y = y0 + 0.1;
        x = x0 + (in-1)*hs;
        [ Guides( gid ), ~, ~, ~ ] = precise.draw.wguide( ax, x+0.1*hs, y, ...
            g, color(gid), 0.8*hs, ...
            'Direction', 'right', 'Width', args.Width );
        g = g + 1;
        gid = gid + 1;
        y = y + 0.8;
        [ Guides( gid ), ~, ~, ~ ] = precise.draw.wguide( ax, x+0.1*hs, y, ...
            g, color(gid), 0.8*hs, ...
            'Direction', 'right', 'Width', args.Width );
        g = g + 1;
        gid = gid + 1;
    end
    if closed
        y = y0 + 0.1;
        x = x0 + (N-1)*hs;
        [ Guides( gid ), ~, ~, ~ ] = precise.draw.circle( ax, ...
            x+0.05*hs, y+0.4, g, 1, ...
            -pi/2, pi/2, ...
            'Color', color( gid ), ...
            'Radius', 0.4, ...
            'Width', args.Width );
    end

    Inputs = gobjects( 4 - 2*closed, 1 );
    Outputs = gobjects( 4 - 2*closed, 1 );
    yd = y0 + 0.1;
    yu = y0 + 0.1 + 0.8;
    xL = x0 - 0.3*hs;
    xR = x0 + 0.3*hs + (N-1)*hs;
    [ ~, Inputs( 1 ), Outputs( 1 ), ~ ] = ...
        precise.draw.IOport( ax, xL, yd, 'Size', 0.1 );
    [ ~, Inputs( 2 ), Outputs( 2 ), ~ ] = ...
        precise.draw.IOport( ax, xL, yu, 'Size', 0.1 );
    switch closed
        case false
            [ ~, Inputs( 3 ), Outputs( 3 ), ~ ] = ...
                precise.draw.IOport( ax, xR, yd, 'Direction', 'left', 'Size', 0.1 );
            [ ~, Inputs( 4 ), Outputs( 4 ), ~ ] = ...
                precise.draw.IOport( ax, xR, yu, 'Direction', 'left', 'Size', 0.1 );
    end

    % Reset hold to old position
    if tf
        hold( ax, 'on' );
    else
        hold( ax, 'off' );
    end
    Tags = [];
end
