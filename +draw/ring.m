function [Guides, Inputs, Outputs, Tags ] = ring( ax, x0, y0, p, ...
    g, mode, color, args )
%RING Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x0 (1,1) double
        y0 (1,1) double
        p (1,1) double { mustBeInteger }
        g (1,1) double { mustBeInteger }
        mode { mustBeMember( mode, ["all-pass", "add-drop"] ) }
        color (:,:) = repmat( ('ymcrgbk')', ...
            ceil( (1 + strcmp(mode, "add_drop") )/7 ), 1 )
        args.Width (1,1) double { mustBePositive } = 5
        args.Gap (1,1) double { mustBePositive } = 0.15
    end
    N = 1 + strcmp(mode, "add-drop");

    % Set hold to true
    tf = ishold( ax );
    hold( ax, 'on' );
    
    % Preallocate memory for graphics objects
    Guides = gobjects( N, 1 );

    y = y0;
    x = x0;
    precise.draw.coupler( ax, x, y+0.15, p );
    y = y + 0.15 + args.Gap;
    p = p + 4;

    [ Guides( 1:2 ), ~, ~, ~ ] = precise.draw.circle( ax, ...
        x, y+0.2, g, N, ...
        'Color', color( 1:2 ), ...
        'Radius', 0.2, ...
        'Width', args.Width );
    y = y + 0.4;
%     g = g + 2;
    
    if N == 2
        precise.draw.coupler( ax, x, y+args.Gap, p );
%         y = y + 0.3;
%         p = p + 4;
    end

    Inputs = gobjects( N, 1 );
    Outputs = gobjects( N, 1 );
    for i = 1:N
        y = y0 + 0.1 + (i-1)*(0.5 + 2*args.Gap);
        xL = x0 - 0.1;
        xR = x0 + 0.1;
        [ ~, Inputs( i ), Outputs( i ), ~ ] = ...
            precise.draw.IOport( ax, xL, y, 'Size', 0.04 );
        [ ~, Inputs( 1+i ), Outputs( 1+i ), ~ ] = ...
            precise.draw.IOport( ax, xR, y, 'Direction', 'left', 'Size', 0.04 );
    end

    % Reset hold to old position
    if tf
        hold( ax, 'on' );
    else
        hold( ax, 'off' );
    end
    Tags = [];
end
