function [outcome] = linear_crow( argsin )
% LINEAR_CROW This function plots the linear spectrum of a CROW structure.
% All parameters are optional. By default the CROW will have two rings.
% 
% Arguments:
%   - 'n' (default 2): number of rings in the CROW.
%   - 'freq' (default 190:0.001:195): frequencies in [THz] at which the
%       spectrum is evaluated.
%   - 'ks' (default sqrt(0.1)): field coupling coefficients; can be
%       scalar or vector of length n+1.
%   - 'radia' (default 7): radius value in [um]; can be a scalar or a
%       vector of length n or 2n.
%   - 'neff' (default 2.4): effective refractive index.
%   - 'ng' (default 4): group index.
%   - 'loss'  (default 2e-4): is the loss value in [dB/um].
%   - 'area' (default 0.25*0.45): waveguide section area in [um^2].

    arguments
        argsin.n (1,1) double { mustBeInteger, mustBePositive } = 2;
        argsin.freq (1,:) double = 190:0.001:195
        argsin.ks (1,:) double = sqrt(0.1)
        argsin.radia (1,:) double = 7;
        argsin.neff (1,1) double = 2.4;
        argsin.ng (1,1) double = 4;
        argsin.loss (1,1) double = 2e-4;
        argsin.area (1,1) double = 0.25*0.45;
    end
    %% crow
    C = precise.optical_system();
    C.add_crow( argsin.n );
    C.add_input( 1 );

    parfun = C.generate_pfe_function();
    [~, NA] = C.get_active_elements();
    NK = C.count.coeffs;
    NP = C.count.ports;
    
    %% adapt parameters to system
    % coupling coefficients
    if isscalar( argsin.ks )
        ks = repelem( argsin.ks, 1, NK );
    elseif length( argsin.ks ) == NK
        ks = argsin.ks;
    else
        error( "'ks' must be either a scalar or a vector of length %d.", NK );
    end
    kcell = repmat( {3;nan;nan;1;"k coeff."}, 1, NK );
    kcell(2:3,:) = num2cell( [1:NK; ks] );
    % radia
    if isscalar( argsin.radia )
        radia = pi .* repelem( argsin.radia, 1, 2*argsin.n );
    elseif length( argsin.radia ) == argsin.n
        radia = pi .* repelem( argsin.radia, 1, 2 );
    else
        error( "'radia' must be either a scalar or a vector of length %d or %d.", ...
            argsin.n, 2*argsin.n );
    end
    rcell = repmat( {4;nan;nan;1;"Radia"}, 1, NA );
    rcell(2:3,:) = num2cell( [1:NA; radia] );

    %% create dataloader
    
    [loader, ~, ~, ~, ~] = precise.dataloader( ...
        1, 1,    1,           1,           "Power", ...
        2, 1,    argsin.freq, 2,           "Frequency", ...
        kcell{:}, ...
        rcell{:}, ...
        5, 1:NA, argsin.neff, 1,           "N_eff", ...
        6, 1:NA, argsin.loss, 1,           "loss [dB/cm]", ...
        7, 1,    argsin.area, 1,           "Area", ...  % waveguide cross-section
        8, 1,    argsin.ng,   1,           "group index" ...
    );

    %% generate linear iterator
    LINfun = precise.iterator.linear( loader, parfun, argsin.freq, NP, NA );
    
    %% Evaluate data
    tic
    [f, o] = LINfun( 1 );
    toc
    fprintf('...for %d points.\n', length( f ) );
    
    %% defining legends
    Legends = compose( {'E%ddown+'; 'E%ddown-'; 'E%dup-'; 'E%dup+'}, 1:argsin.n);
    Legends = [{'in'; 'th'}; Legends(:); {'add'; 'drop'} ];
    Title = sprintf('CROW with %d ring', argsin.n);
    
    %% plotting figure
    figure();
    precise.plot_utils.suppressing_linear_spectrum( f, o .* conj(o), ...
        Title, Legends );

    %% output
    if nargout
        outcome = {f, o, LINfun, loader, parfun, C, argsin};
    end
end
