function [outcome] = linear_ring( argsin )
% LINEAR_RING This function plots the linear spectrum of a single ring.
% All parameters are optional.
% 
% Arguments:
%   - 'freq' (default 190:0.001:195): frequencies in [THz] at which the
%       spectrum is evaluated.
%   - 'ks' (default sqrt(0.1)): field coupling coefficients; can be
%       scalar or vector of length 2.
%   - 'radia' (default 7): radius value in [um]; can be a scalar or a
%       vector of length n or 2n.
%   - 'neff' (default 2.4): effective refractive index.
%   - 'ng' (default 4): group index.
%   - 'loss'  (default 2e-4): is the loss value in [dB/um].
%   - 'area' (default 0.25*0.45): waveguide section area in [um^2].

    arguments
        argsin.freq (1,:) double = 190:0.001:195
        argsin.ks (1,:) double = sqrt(0.1)
        argsin.radius (1,1) double = 7;
        argsin.neff (1,1) double = 2.4;
        argsin.ng (1,1) double = 4;
        argsin.loss (1,1) double = 2e-4;
        argsin.area (1,1) double = 0.25*0.45;
        argsin.type { mustBeMember( argsin.type, ...
            ["All Pass", "Add Drop"] ) } = "Add Drop"
    end
    %% elaborate input arguments
    add_drop = strcmp( argsin.type, "Add Drop" );
    
    %% single ring
    R = precise.optical_system();
    R.add_ring( 1 + add_drop );
    R.add_input( 1 );
    
    parfun = R.generate_pfe_function();
    [~, NA] = R.get_active_elements();
    NK = R.count.coeffs;
    NP = R.count.ports;
    
    %% adapt parameters to system
    % coupling coefficients
    if isscalar( argsin.ks )
        ks = repelem( argsin.ks, 1, NK );
    elseif length( argsin.ks ) == NK
        ks = argsin.ks;
    else
        error( "'ks' must be either a scalar or a vector of length %d.", NK );
    end
    kcell = repmat( {3;nan;nan;1;"k coeff."}, 1, NK );
    kcell(2:3,:) = num2cell( [1:NK; ks] );
    % radia
    Radia = (2-add_drop)*argsin.radius*pi;
    
    %% create dataloader
    
    [loader, ~, ~, ~, ~] = precise.dataloader( ...
        1, 1,    1,           1, "Power", ...
        2, 1,    argsin.freq, 2, "Frequency", ...
        kcell{:}, ...            "k coeff."
        4, 1:NA, Radia,       1, "Radia", ...
        5, 1:NA, argsin.neff, 1, "N_eff", ...
        6, 1:NA, argsin.loss, 1, "loss [dB/cm]", ...
        7, 1,    argsin.area, 1, "Area", ...  % waveguide cross-section
        8, 1,    argsin.ng,   1, "group index" ...
    );

    %% generate linear iterator
    LINfun = precise.iterator.linear( loader, parfun, argsin.freq, NP, NA);
    
    %% Evaluate data
    tic
    [f, o] = LINfun( 1 );
    toc
    fprintf('...for %d points.\n', length( f ) );
    
    %% defining legends
    if add_drop
        Legends = {'in'; 'th'; 'E1down+'; 'E1down-'; 'add'; 'drop'; 'E2up+'; 'E2up-'};
        Title = 'Single ring in Add-Drop configuration';
    else
        Legends = {'in'; 'th'; 'E1+'; 'E1-'};
        Title = 'Single ring in All-Pass configuration';
    end
    
    %% plotting figure
    figure();
    precise.plot_utils.suppressing_linear_spectrum( f, o .* conj(o), ...
        Title, Legends );

    %% output
    if nargout
        outcome = {f, o, LINfun, loader, parfun, R, argsin};
    end
end
