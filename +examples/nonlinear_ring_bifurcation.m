%% NONLINEAR_RING
%   This script is provided as example in the solution of a microring
%   resonator with one constant input signal.
%   The system is solved from 0 to 2000 [ns] every 100 [ps], for each
%   frequency around the resonance (191.5375 THz) from -25 to +15 [GHz]
%   every 5 [GHz]. The coupling coefficients are all equal and their value
%   is between 0.17 and 0.25 every 0.02. The ring have radius of 7 [um].
%   The waveguide section is 0.25 * 0.45 um^2, the effective index is 2.4,
%   the group index is 4, and the linear loss is 2e-4 [dB/um].
%   
%   From the system solution, the average value and min/max values of the
%   timeseries are saved. Moreover, the period of the most important FFT
%   component and the extremal points are extracted for each timeserie.
%   
%   At the end of the analysis, several plots are shown and can be update
%   with the update( j ) method for each plot, requiring only the
%   combination index j as argument.
%   Example:
%       tic;
%       si = {6, 10, 3, 1, 1};
%       D.update( si );
%       V.update( si ); P.update( si );
%       EC.update( si); SC.update( si );
%       toc
%   
%   This examples take ~17s loading the parpool and ~500s (8'20")
%   evaluating the system dynamic on an AMD R5 2500U laptop with 4 workers.

clear

%% system

S = precise.optical_system();
S.add_ring( 2, true );
S.add_input( 1 );

%% generate parametric field evaluator function
%   and get the active waveguides.

parfun = S.generate_pfe_function();
[active, NA] = S.get_active_elements();

%% generate loader function and number of combinations

powers = logspace(0.45,1.10,15);
freqs  = 191.5375 + 1e-3.*(-25:5:+15);
Kcoeff = .17:0.02:.25;
input_func = @(t) 1-exp(-t/10e3);

[loader, N, sources, j2ind, ind2j] = precise.dataloader( ...
    1, 1,       powers,     2, "Power", ...
    2, 1,       freqs,      1, "Frequency", ...
    3, 1:2,     Kcoeff,     3, "k coeff.", ...
    4, 1:2,     7*pi,       4, "Radia", ...
    5, 1:NA,    2.4,        5, "N_eff", ...
    6, 1:NA,    2e-4,       5, "loss [dB/cm]", ...
    7, 1,       0.25*0.45,  5, "Area", ...  % waveguide cross-section
    8, 1,       4,          5, "group index", ...
    9, 1,       45e-6,      5, "thermal constant", ...
    10, 1,      300e-6,     5, "free carrier constant" ...
);

%% generate iterator functions
% nonlinear iterator
time = 0:0.1e3:2000e3; % [ps]
initial_assumption = zeros( 2*NA, 1);
ode_options = odeset( ...
    'RelTol', 1e-6, ...
    ...'AbsTol', repelem( [0.1;1], nActive, 1), ...
    'NonNegative', 1:2*NA ...
);
NLfun = precise.iterator.nonlinear( loader, parfun, S.count.ports, active, ...
    input_func, precise.materials.silicon, time, initial_assumption, ...
    ode_options);

% linear iterator
LINfun = precise.iterator.linear( loader, parfun, freqs, S.count.ports, NA);

clear time initial_assumption ode_options

%% Preallocation of data arrays
Avg = nan(2*NA, N);
Max = nan(2*NA, N);
Min = nan(2*NA, N);
% T   = nan(2*NA, N);
% ext1 = cell(2*NA, N);
ext2v = cell(S.count.ports, N);
ext2c = cell(S.count.ports, N);

%%
if isempty( gcp('nocreate') )
    tic
    parpool;
    toc
    fprintf( "... time spent loading the parallel pool.\n\n" );
else
    fprintf( "Parallel pool already started.\n\n" );
end
fprintf( "Starting the data evaluation of %d combinations. (%s)\n", N, ...
    datestr(now, 'HH:MM:SS') );
tic
parfor j = 1:N
    
    [sol, t, fdeval] = feval( NLfun, j);
    
    % restrict analysis to second half of timeline
    [x, y] = precise.analysis.select_last_half( sol.x, sol.y );
    % select larger 2^N points in the second half of timeline
    t = precise.analysis.select_pow2_pts( t );
    % evaluate DT, DN, dn, and optical fields
    [DT, DN, ~, opts] = fdeval( sol, t );
    
    Avg(:,j) = trapz( x, y, 2) ./ ( x(end) - x(1) );
    Max(:,j) = max( y, [], 2);
    Min(:,j) = min( y, [], 2);
    
    % period of main FFT component
%     T(:,j) = precise.analysis.periodFFT( t, [DN;DT], 0.05, false );

    % extremal points for DT and DN
%     ext1(:,j) = [ precise.analysis.extremals( DT, 1e-3); ...
%                   precise.analysis.extremals( DN, 1e-3) ];

    % extremal points for internal field
    [v, c] = precise.analysis.extremals_count( opts, 1e-2);
    ext2v(:,j) = v; % extremals values
    ext2c(:,j) = c; % extremals counts

end
toc

%% Plot

D = precise.plot_utils.dashboard( {Avg, Min, Max}, NLfun, LINfun, ind2j, ...
    @( ax, col ) precise.draw.scissor( ax, 0, 0, 1, 1, 1, false, col ), ...
    'Title', 'Dashboard' );

%% Visibility (DN and DT)
subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );

V = precise.plot_utils.array_plot( ...
    (Max-Min)./(Max+Min), ... data
    sources, j2ind, 2, ... sources, index functions, and nr of rows
    'Subtitles', subtitles(:), ...
    'Title', "Visibility Contourf Plot" ...
);

clear subtitles

%% Period of the main FFT component (DN and DT)
% subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );
%
% P = precise.plot_utils.array_plot( ...
%     log10(T), ... data
%     sources, j2ind, 2, ... sources, index functions, and nr of rows
%     'Subtitles', subtitles(:), ...
%     'Title', "Period Contourf Plot" ...
% );
%
% clear subtitles

%% Extremals with counts (optical fields)
ch = [2 3 6 7];

EC = precise.plot_utils.extremal_count( ...
    ext2v(ch,:), ext2c(ch,:), ... data
    sources, j2ind, 2, ... sources, index functions, and nr of rows
    'Subtitles', {'Through'; 'E1down+'; 'drop'; 'E2up-'}, ...
    'Title', "Extremals Contourf Plot" ...
);

clear ch

%% Self pulsing reduced in power (optical fields)
ch = [2 3 6 7];
sp = cellfun( @(v) length(v) > 1 , ext2v(ch, :) );

[ sp, sp_sources, sp_j2ind, ~ ] = precise.analysis.reduce_dim( sp, sources, 2, 'first' );

SP = precise.plot_utils.array_plot( ...
    sp, ... data
    sp_sources, sp_j2ind, 2, ... sources, index functions, and nr of rows
    'Subtitles', {'Through'; 'E1down+'; 'drop'; 'E2up-'}, ...
    'Title', "Self-Pulsing Plot", ...
    'ReducedDim', 2 ...
);

clear ch
