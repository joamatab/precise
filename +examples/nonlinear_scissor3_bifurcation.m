%% NONLINEAR_SCISSOR
%   This script is provided as example in the solution of a SCISSOR
%   structure with three microring resonators with one constant input
%   signal.
%   The system is solved from 0 to 1000 [ns] every 100 [ps], for each
%   frequency around the resonance (191.5375 THz) from -25 to +25 [GHz]
%   every 2.5 [GHz]. The coupling coefficients are all equal and their
%   value is between 0.17 (and 0.25 every 0.02). The ring have radius of 7
%   [um]. The waveguide section is 0.25 * 0.45 um^2, the effective index is
%   2.4, the group index is 4, and the linear loss is 2e-4 [dB/um].
%   
%   From the system solution, the average value and min/max values of the
%   timeseries are saved. Moreover, the period of the most important FFT
%   component and the extremal points are extracted for each timeserie.
%   
%   At the end of the analysis, several plots are shown and can be update
%   with the update( j ) method for each plot, requiring only the
%   combination index j as argument.
%   Example:
%       tic;
%       si = {2, 3, 1, 1, 1};
%       D.update( si );
%       V.update( si ); P.update( si );
%       EC.update( si); SC.update( si );
%       toc
%   
%   This examples take ~15s loading the parpool and around 900s (15')
%   evaluating the system dynamic on an AMD R5 2500U laptop with 4 workers.

clear

%% system
NR = 3;
closed = false;

S = precise.optical_system();
S.add_scissor( NR, 'full', closed );
S.add_input( 1 );

%% generate parametric field evaluator function
%   and get the active waveguides.

parfun = S.generate_pfe_function();
[active, NA] = S.get_active_elements();

%% generate loader function and number of combinations

powers  = logspace(0.45,1.10,12);
freqs   = 191.5375 + 1e-3.*(-24:3:+24);
R0      = pi*7;
dR      = 1 + 1e-5.*(1:2:3);
IDL     = 2*NR+(1:2*NR-2+closed);
Kcoeff  = .17:0.1:.25;
input_func = @(t) 1-exp(-t/10e3);

[loader, N, sources, j2ind, ind2j] = precise.dataloader( ...
    1, 1,       powers,             2, "Power", ...
    2, 1,       freqs,              1, "Frequency", ...
    3, 1:2*NR,  Kcoeff,             3, "k coeff.", ...
    4, 1:2,     R0.* dR(1),            4, "Radius 1 (&3)", ...
    4, 3:4,     R0,                 5, "Radius 2", ...
    4, 5:6,     R0.* fliplr(dR(end)),    4, "Radius 3 (&1)", ...
    4, IDL,     R0.*1.5,            5, "Lengths", ...
    5, 1:NA,    2.4,                5, "N_eff", ...
    6, 1:NA,    2e-4,               5, "loss [dB/cm]", ...
    7, 1,       0.25*0.45,          5, "Area", ...  % waveguide cross-section
    8, 1,       4,                  5, "group index", ...
    9, 1,       45e-6,              5, "thermal constant", ...
    10, 1,      300e-6,             5, "free carrier constant" ...
);

%% Check linear spectrum
% on a finer frequency grid
ni = 4;
precise.examples.linear_scissor( "area", 0.25*0.45, "ks", Kcoeff(1), ...
    "freq", interp1( freqs, 1:1/ni:length(freqs) ), ...
    "loss", 2e-4, "n", NR, "neff", 2.4, "ng", 4, ...
    "path", R0.*1.5, "radia", R0/pi.*[dR(1), 1, dR(end)], ...
    "closed", closed )
drawnow;
cmd = input( 'Please check the linear spectrum. Continue? [Y/N] ', 's' );
if cmd ~= 'Y'
    return
end
fprintf('\n');

%% generate iterator functions
% nonlinear iterator
time = 0:0.05e3:1000e3; % [ps]
initial_assumption = zeros( 2*NA, 1);
ode_options = odeset( ...
    'RelTol', 1e-6, ...
    'NonNegative', 1:2*NA ...
);
NLfun = precise.iterator.nonlinear( loader, parfun, S.count.ports, active, ...
    input_func, precise.materials.silicon, time, initial_assumption, ...
    ode_options);

% linear iterator
LINfun = precise.iterator.linear( loader, parfun, freqs, S.count.ports, NA);

clear time initial_assumption ode_options

%% Preallocation of data arrays
Avg = nan(2*NA, N);
Max = nan(2*NA, N);
Min = nan(2*NA, N);
T   = nan(2*NA, N);
% ext1 = cell(2*NA, N);
ext2c = cell(S.count.ports, N);
ext2v = cell(S.count.ports, N);

if isempty( gcp('nocreate') )
    tic
    parpool;
    toc
    fprintf( "\n... time spent loading the parallel pool.\n\n" );
else
    fprintf( "\nParallel pool already started.\n\n" );
end
fprintf( "Starting the data evaluation of %d combinations. (%s)\n", N, ...
    datestr(now, 'HH:MM:SS') );
tic
parfor j = 1:N
    
    [sol, t, fdeval] = feval( NLfun, j);
    
    % restrict analysis to second half of timeline
    [x, y] = precise.analysis.select_last_half( sol.x, sol.y );
    % select larger 2^N points in the second half of timeline
    t = precise.analysis.select_pow2_pts( t );
    % evaluate DT, DN, dn, and optical fields
    [DT, DN, ~, opts] = fdeval( sol, t );

    Avg(:,j) = trapz( x, y, 2) ./ ( x(end) - x(1) );
    Max(:,j) = max( y, [], 2);
    Min(:,j) = min( y, [], 2);
    
    T(:,j) = precise.analysis.periodFFT( t, [DN;DT], 0.05, false );

    % extremal points for DT and DN
%     ext1(:,j) = [ precise.analysis.extremals( DT, 1e-3); ...
%         precise.analysis.extremals( DN, 1e-3) ];

    [values, counts] = precise.analysis.extremals_count( opts, 1e-3 );
    ext2v(:,j)  = values;
    ext2c(:,j) = counts;
end
toc
fprintf( "...for %d combinations.\n\n", N);

%% Plot

D = precise.plot_utils.dashboard( {Avg, Min, Max}, NLfun, LINfun, ind2j, ...
    @( ax, col ) precise.draw.scissor( ax, 0, 0, 1, 1, NR, closed, col ), ...
    'Title', 'Dashboard' );

%% Visibility (DN and DT)
subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );

V = precise.plot_utils.array_plot( ...
    (Max-Min)./(Max+Min), ... data
    sources, j2ind, 4, ... sources, index functions, and nr of rows
    'Subtitles', subtitles(:), ...
    'Title', "Visibility Contourf Plot" ...
);

clear subtitles

%% Period of the main FFT component (DN and DT)
subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );

P = precise.plot_utils.array_plot( ...
    log10(T), ... data
    sources, j2ind, 4, ... sources, index functions, and nr of rows
    'Subtitles', subtitles(:), ...
    'Title', "Period Contourf Plot" ...
);

clear subtitles
%% Extremals with counts (optical fields)
ch = [2 3 6 7 10 11 14 15 18 19 22 23];
subtitles = compose( {'in%d'; 'th%d'; 'E%ddown+'; 'E%ddown-'; ...
        'add%d'; 'drop%d'; 'E%dup+'; 'E%dup-' }, 1:NR);

EC = precise.plot_utils.extremal_count( ...
    ext2v(ch,:), ext2c(ch,:), ... data
    sources, j2ind, 3, ... sources, index functions, and nr of rows
    'Subtitles', subtitles( ch ), ...
    'Title', "Extremals Contourf Plot" ...
);

clear ch

%% Self pulsing reduced in power (optical fields)
ch = [2 3 6 7 10 11 14 15 18 19 22 23];
subtitles = compose( {'in%d'; 'th%d'; 'E%ddown+'; 'E%ddown-'; ...
        'add%d'; 'drop%d'; 'E%dup+'; 'E%dup-' }, 1:NR);
    
sp = cellfun( @(v) length(v) > 1 , ext2v(ch, :) );

[ sp, sp_sources, sp_j2ind, ~ ] = precise.analysis.reduce_dim( sp, sources, 2, 'first' );

SP = precise.plot_utils.array_plot( ...
    sp, ... data
    sp_sources, sp_j2ind, 3, ... sources, index functions, and nr of rows
    'Subtitles', subtitles( ch ), ...
    'Title', "Self-Pulsing Plot", ...
    'ReducedDim', 2 ...
);

clear ch
