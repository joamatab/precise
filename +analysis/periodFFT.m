function periods = periodFFT( time, values, threshold, verbose )
%PERIOD_VIA_FFT Extract the period of the most prominent frequency
%   component using the FFT algorithm 
    arguments
        time (1,:) double
        values (:,:) double
        threshold (1,1) double = 0.1
        verbose logical = false
    end
    
    T  = time(2) - time(1); % in [ps]
    Fs = 1/T;               % in [THz]
    L  = length(time);
    M  = mean( values, 2 );
    
    if verbose
        figure()
        plot( time, values )
        title('Original signals')
        xlabel('time [ps]')
        ylabel('X(t)')
        legend
    end
    
    % Remove the DC component
    Y_AC = values./M - 1;
    % Normalize for the sum of square modulus
    Y_Norm = Y_AC ./ sum( Y_AC .* conj(Y_AC) );

    % execute the fft of the signal
    Y = fft( Y_Norm, [], 2);
    
    P2 = abs( Y/L );

    % deprecated because slower
%     P1 = P2(:,1:L/2+1);
%     P1(:,2:end-1) = 2*P1(:,2:end-1);

    % this is slightly faster (~20%) than the two lines above
    P1 = [P2(:,1), 2*P2(:,2:L/2), P2(:,L/2+1)];
    
    fvec = Fs*(0:(L/2))/L;
        
    if verbose
        figure()
        plot( fvec, P1)
        title('Single-Sided Amplitude Spectrum of X(t)')
        xlabel('f (THz)')
        ylabel('|P1(f)|')
        legend
    end
    
    [P, I] = max( P1, [], 2 );
    periods = 1./fvec( I.' );
    periods( P < threshold ) = nan;
end