function [value, count] = extremals_count( data, tolerance )
%EXTREMALS_COUNT Returns the local maxima and minima as well as their
%   count. Values closer than a given tolerance are considered as a single
%   value.
    arguments
        data
        tolerance
    end
    
    N = size( data, 1);
    idmax = islocalmax( data, 2 );
    idmin = islocalmin( data, 2 );
    
    id = logical(idmin + idmax);
    
    value = cell(N,1);
    count = cell(N,1);
    for j = 1:N
        row = data( j, id(j,:) );
        if isempty( row )
            value(j,1) = {data(j,end)};
            count(j,1) = {1};
        else
            [C, ~, IC] = uniquetol( row, tolerance );
            value(j,1) = { C };
            count(j,1) = { histcounts(IC) };
        end
    end
end
