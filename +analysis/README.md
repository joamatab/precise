# Aanalysis subpackage

The `precise.analysis` subpackage provides a set of useful functions that
extract a figure of merit, usually a scalar value, from each time series
evaluated in the nonlinear study.