function [aload, N, SRC, j2ind, ind2j] = dataloader( cell_id, ...
    elem_id, data, order, labels )
%DATALOADER Returns the function handle of the loader function, which
%retrieves a unique cell array containing the j-th combination of
%parameters, given an integer index 'j'. The parameters space is obtained
%by the taking the carthesian product of all the data vectors.
%   aload = DATALOADER( _, _, _, _ ) returns the handle to the args_loader
%       function only.
%   [aload, n, dim, sources, findex] = DATALOADER( _, _, _, _ ) returns
%       the handle to args_loader and the number N of total combinations,
%       the SRC table containing the actual parameters and their labels,
%       and the handles to the indexing functions 'indexing' and
%       'deindexing'.
%
%   Outputs:
%       - ALOAD, function handle for args = args_loader( j ): is a function
%           of a single integer 'j', that returns a combination of system
%           parameters in a cell array of scalars or numerical arrays.
%       - N, double: is the total number of combinations.
%       - SRC, table: contains the value and the labels of the actual
%           parameters.
%       - J2IND, function handle for {i1, ..., im} = indexing( j ): is a
%           function that, given a pos. integer 'j', returns a cell array
%           of indexes {i1, ..., im} each pointing to one of the array
%           contained in 'sources'.
%       - IND2J, function handle for j = deindexing(i1, ..., im): is the
%           inverse function of j2ind.
%
%   Arguments ( Repeating ):
%       - cell_id, pos. integer: specifies the place in the cell array,
%           as in `args{ cell_id }`
%       - elem_id, pos. integer col. vector or scalar: specifies the
%           place in the array within a cell, as in `args{ cell_id }[
%           elem_id ]`
%       - data, row vector: contains the actual values of the parameter
%       - order, pos. integer row vector or scalar: define the order in
%           which the parameters are cycled with 'j'. The lowest order is
%           cycled first. The same order can define dependent parameters,
%           while different orders define independent parameters. This is
%           similar to nesting for loops, where the lowest order is the
%           inner loop and the higher order is the outer loop.
%       - label, string: label string of the data vector.
%
%   Example: consider
%       [aload, n, dim, sources, ind_fh] = dataloader( ...
%           1, 1,   [p1, p2, p3, p4], 1,   "power", ...
%           2, 1,   [f1, f2, f3],     2,   "frequency", ...
%           3, 1:2, [k1, k2],         3,   "k coeff.", ...
%           4, 1:3, [R1, R2, R3],     4:6, "radius", ...
%           5, 1:2, [L1, L2],         7,   "these lengths", ...
%           5, 3:4, [l1, l2],         7,   "other lengths" ...
%       )
%   Each row will define what the cell returned by 'aload' will contain:
%     1) the "power" coefficient will be in the first element (1) of the
%       first cell (1) and its value will be one among [p1, p2, p3, p4].
%       Its order is 1, so it will cycle more frequently than the others.
%     2) the "frequency" coefficient will be the first element (1) of the
%       second cell (2) and its value will be one among [f1, f2, f3]. Its
%       order is 2, so it will cycle after "power": in the nested for loop
%       analogy the "power" for loop is inside the "frequency" one.
%     3) the "k coeff." will be the first and second element (1:2) in the
%       third cell (3) and their values will be taken from [k1, k2].
%       Since only one is defined, each element in the cell will cycle at
%       the same time.
%     4) the "radius" will populate the first, second and third elements
%       (1:3) of the fourth cell (4) and their values will belong to [R1,
%       R2, R3]. Since three orders (4:6) are defined, all the radia will
%       cycle independently from the other. Their labels will be "radius
%       1", "radius 2", and "radius 3" relatively for the three elements.
%     5-6) the two "lengths" coefficients will populate the first and the
%       second elements (1:2) and the third and the fourth elements (3:4)
%       in the fifth cell (5). Their values will belong to [L1, L2] and to
%       [l1, l2] respectively. Since they both have the same order, they
%       will cycle at the same time, similarly to the third order.
%   Moreover:
%       - aload(1) will return the 1st combination or parameters in a cell
%           array { p1, f1, [k1; k1], [R1; R1; R1], [L1; L1; l1; l1] }.
%       - N is equal to 4*3*2*3*3*3*2 = 1296 combinations
%       - SRC is the 7x3 table:
%             id       value             label
%             __    ____________    _______________
%             1     {14 double}    "power"
%             2     {13 double}    "frequency"
%             3     {12 double}    "k coeff."
%             4     {13 double}    "radius 1"
%             5     {13 double}    "radius 2"
%             6     {13 double}    "radius 3"
%             7     {12 double}    "these lengths"
%           No values from "those lengths" are prensent because they shares
%           the same order as "these lengths". When two elements share the
%           same order, only the first one is considered in the SRC table.
%       - J2IND( j ) returns a cell array of seven integer numbers. The
%           i-th number is the index used to cycle all the elements
%           assigned to the i-th order.
%       - IND2J( i1, i2, i3, i4, i5, i6, i7 ) will require seven arguments
%       ( positive integers ) and it will return a single index j.
    arguments ( Repeating )
        cell_id (1,1) {mustBeInteger, mustBePositive }
        elem_id (:,1) {mustBeInteger, mustBePositive }
        data (1,:)
        order (1,:) {mustBeInteger, mustBePositive, ...
            mustBeScalarOrSameLength( order, elem_id ) }
        labels (1,:) string
    end
    
    k = 1;
    for j = 1:length(order)
        if isscalar( order{j} )
            k = k+1;
        else
            L = length( order{j} );
            cell_id = repeatCellElement( cell_id, k, L);
            elem_id = splitCellElement( elem_id, k);
            data = repeatCellElement( data, k, L);
            labels = repeatCellElement( labels, k, L);
            labels(k:k+L-1) = cellfun(@(x,y) append(x, " ", num2str(y)),...
                labels(k:k+L-1), num2cell( 1:L ), 'UniformOutput', false);
            k = k+L;
        end
    end
    clear k j L

    [~, io, iu] = unique( cell2mat( order ) );
    sizes = cellfun( @length, data( io ) );
    
    function indexes = indexing(j)
        %INDEXING Returns the cell array containing the unique sub-indeces
        %combination corresponding to the index j. Assumes a rectangular
        %n-dimensional matrix of dimensions given by the nonlocal variable
        %'sizes'.
        indexes  = cell( 1, length(sizes) );
        [indexes{:}] = ind2sub( sizes, j ) ;
    end
    function J = deindexing( I )
        J = sub2ind( sizes, I{:} );
    end
    
    function args = args_loader( j )
        %ARGS_LOADER Returns the j-th combination of the parameters in a
        %cell array 'args', containing scalars and/or column vectors.
        args = cell( length( unique( cell2mat(cell_id) ) ), 1);
        
        indexes = indexing( j );
        indexes = indexes( iu );
        
        for i=1:length(cell_id)
            args{ cell_id{i} }( elem_id{i}, 1 ) = deal( data{i}( indexes{i} ) );
        end
    end
    
    aload = @args_loader;
    if nargout > 1
        N = prod( sizes );
        SRC = table( (1:length(io))', data(io)', [labels{io}]', ...
            'VariableNames', ["id", "value", "label"] );
        j2ind = @indexing;
        ind2j = @deindexing;
    end
end

function tf = mustBeScalarOrSameLength( x, y )
    %MUSTBESCALARORSAMELENGTH Returns true if x is scalar or has the same
    %length of y.
    if length( x ) == 1
        tf = true;
    elseif length( x ) == length( y )
        tf = true;
    else
        tf = false;
    end
end

function Cell = repeatCellElement( Cell, pos, N)
    %REPEATCELLELEMENT Repeat N times the element at position 'pos' in the
    %cell array 'Cell'.
    Cell = [Cell(1:pos-1), repelem( Cell(pos), 1, N ), Cell(pos+1:end)];
end

function Cell = splitCellElement( Cell, pos )
    %SPLITCELLELEMENT Split vector of length N in cell at position 'pos' in
    %N cells containing one vector's element each.
    Cell = [Cell(1:pos-1), num2cell( Cell{pos} )', Cell(pos+1:end)];
end
