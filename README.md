# Precise

The Photonic hybRid EleCtromagnetic SolvEr (PRECISE) is a Matlab based library to model large and complex photonics integrated circuits.


## Documentation
The library documentation can be found at the [repository wiki](../../wikis/Home)
