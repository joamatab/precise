function add_splitter_3x3( obj, k1, k2 )
%ADD_SPLITTER_3X3 Create a 3x3 splitter node.
%   Create a 6-port node with two sides, three ports per side. Each side is
%   linked to the other with the following matrix.
%   |right_above|  = |kk ku t1||left_above|
%   |right_center| = |kd t0 ku||left_center|
%   |right_below|  = |t1 ku kk||left_below|
%
%   The actual matrix containing the link coefficients is generated with
%   the `matrix_coeff_splitter_3x3` static method.
    arguments
        obj
        k1 = obj.get_new_coeff()
        k2 = obj.get_new_coeff()
    end
    
    % get current port count
    p = obj.count.ports;
    
    % create a node with 2 input and 2 output ports, total of 4.
    obj.add_node( 6 );
    
    % generate the link matrix
    K = obj.matrix_coeff_splitter_3x3( k1, k2 );
    
    % Add symmetric link
    obj.add_link( p+[1;2;3;4;5;6], p+[1;2;3;4;5;6], K );
end

