function add_taiji_resonator( obj, active )
%ADD_TAIJI_RESONATOR Add a TAJI resonator module to the optical system
%   Detailed explanation goes here
    arguments
        obj
        active logical = true
    end
    
    % Get current port count
    p = obj.count.ports;
    
    % Add Taiji microring
    obj.add_ring( 3, active );
    
    % Add path inside the microring
    obj.add_guide( p+6, p+10, active );
    
end
