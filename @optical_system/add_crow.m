function add_crow( obj, n )
%ADD_CROW Add a CROW module to the optical system
%   Detailed explanation goes here
    arguments
        obj
        n { mustBeInteger, mustBeNonnegative } = 2
    end

    % Aet current port count
    p = obj.count.ports;

    % Add first splitter
    obj.add_splitter_2x2( );

    % Add other splitters
    for i = 1:n
        obj.add_splitter_2x2( );
        
        % Connect splitters with waveguide of the two halves of ring
        j = 4*i+p;
        obj.add_guide( j-1, j+2, true );
        obj.add_guide( j+1, j, true );
    end
end

