function M = matrix_coeff_splitter_2x2( symbol )
%MATRIX_COEFF_SPLITTER_2X2 Generate coefficient matrix for a 2x2 splitter
%with one symbolic coefficient 'symbol'.

    % Get short notation for transmission t and reflection k
    t = sqrt(1-symbol.^2);
    k = 1i.*symbol;
    
    % Create matrix
    M = [ 0 t k 0 ;
          t 0 0 k ;
          k 0 0 t ;
          0 k t 0 ];
end