function pfun = nonlinear( loader, parfun, nPorts, active, input_func, ...
    material, time, initial_assumption, ode_options )

    sq = @(U) U .* conj(U);

    function [sol, t, fdeval] = step( j )
        args = feval( loader, j);

        [f, fopt, fdn] = precise.iterator.ode_step( ...
            parfun, active, ...
            input_func, args{:}, ...
            material ...
        );

        sol = ode23( f, time, initial_assumption, ode_options);
        
        function [DT, DN, dn, opts] = ftotal( sol, t )
            nActive = size(sol.y, 1)/2;
            data = deval( sol, t );
            DT = data( 1:nActive, : );
            DN = data( nActive+1:end, :);

            dn = fdn( DT, DN, zeros( size(DN) ) );
            opts = nan( nPorts, length(t) );
            for i=1:length(t)
                opts(:,i) = sq( fopt( input_func( t(i) ), dn(:,i) ) );
            end
        end
        if nargout > 1
            t = time;
            fdeval = @ftotal;
        end
    end
    
    pfun = @step;
end