function [ode_step, opt_fields, res_dn] = ode_step( par_field_eval, ...
    active_guides, waves, power, fp, k, Length, Neff, dBum_Loss, Area, ...
    ng, G_th, G_fc, material)
    arguments
        par_field_eval
        active_guides (:,2) double
        waves (:,1) % cell
        power (:,1) % cell
        fp double
        k (:,1) double
        Length (:,1) double
        Neff (:,1) double
        dBum_Loss (:,1) double
        Area (:,1) double
        ng (:,1) double
        G_th (:,1) double % = 45e-6;
        G_fc (:,1) double % = 300e-6;
        material (:,1)
    end
    %GENERATE_ODE_STEP_FUNCTION

    sq = @(U) U .* conj(U);
    sqpower = sqrt( power );
    
    c0 = 299.792458;       % [um/ps] c_0, speed of light in vacuum
    e0 = 8.854187817e-18;  % [F/um] epsilon_0, vacuum permittivity
    hbar = 1.054571800e-7; % [fJ*ps] h_bar, reduced Planck constant

    % refractive index imaginary part: losses
    PLoss   = (dBum_Loss ./ 10 ) .* log( 10 ); % [1/um] log( 10.^(dBLoss/10));
    % Group index approximation of the effective index
    f0 = c0/1.550;         % [THz] defining f0 as reference for neff & ng
    neff = f0/fp .* ( Neff + ng .* ( fp - f0 ) ./ f0 ) ...
        + 1i*PLoss/2 * c0/(2*pi*fp);
    
%     nInputs = length(waves);
    nActive = size(active_guides, 1);
    
    %% Variable inputs
    % Material
    n0   = material.n0;
    n2   = material.n2;
    bTPA = material.bTPA;
    sFCD = material.sFCD;
    sFCA = material.sFCA;
    toe  = material.toe;
    
    % Geometries
    Volumes = Length .* Area; % [um]^3
    Masses  = Volumes .* material.density; % [g] mass of each element
    MCP     = Masses .* material.spec_heat; % [J/K] thermal capacity
                        ... [J/K] = [g*J/g/K] = Mass * Specific_Heat
    % Coefficients
    GammaN = 0.998;     % TO BE DEFINED, from FEM simulations!
    GammaV = 1.25;      % TO BE DEFINED, from FEM simulations!
    gI   = bTPA * c0^2 ./ ( n0^2 .* Volumes .* GammaV );
    G_int = PLoss * c0 / ng / 2;    % field decay rate
    fca = c0 * sFCA / (2*n0) * GammaN;
    Nconst = c0^2 * bTPA / (2 * n0^2 * hbar);
    Nfreq  = 1 ./ (2*pi * fp .* Volumes .* Volumes .* GammaV );
    
    field_to_energy = sqrt( Length ./ (c0 / ng) );
    
%     Gk      = k.^2 .* c0 ./ (2*Length .* ng );
%     gR   = c0 * n2 ./ ( n0^2 * Volumes );
%     GammaT = 0.93;      % TO BE DEFINED, from comsol!
%     DDDN = sFCD / n0 * GammaN;
%     DDDT = toe * GammaT / n0;
    
    %% Nested functions
    function active_fields = optical_fields( in, dn )
        %OPTICAL_FIELDS Evaluates the optical field in the system, given the
        %input fields and the nonlinear refractive index variation.
        %   Inside this function, the `par_field_eval` is populated with
        %   the necessary parameters.
        
        % Evaluate fields
        all_fields = par_field_eval( c0, fp, in, k, Length, neff, dn );
        
        % Sum together field in each (active) waveguide.
        active_fields = sum( all_fields( active_guides ), ...
            2 - (size(active_guides,1) == 1) );
        % this is because MATLAB does not operate in a consistent manner...
    end
    function dn = resonance_detuning( DT, DN, U )
        %RESONANCE_DETUNING Evaluates the nonlinear detuning of the
        %refractive index due to thermal, free carrier, and optical
        %effects.
        
        k0 = 2*pi * fp / c0;

        dn_opt = n2.*sq(U) + 1i.*bTPA.*sq(U)/2/k0;
        dn_fc  = sFCD.*DN + 1i.*sFCA.*DN/2/k0;
        dn_th  = toe .* DT;
        
        dn = dn_opt + dn_fc + dn_th;
    end
    function dDTdt = d_temperature(DT, DN, U)
        %D_TEMPERATURE Evaluates the differential increase in temperature
        %difference due to thermal, free carrier, and optical effects.
        
        % Total loss G coefficient: FCA + TPA + Internal
        Gloss = fca .* DN + gI .* sq(U) + G_int;

        % evaluate the power absorbed
        P_abs = (2 * Gloss) .* sq(U);

        dDTdt = -G_th*DT + P_abs./MCP;
    end
    function dDNdt = d_freecarriers(DN, U)
        %D_FREECARRIERS Evaluates the differential increase in free charge
        %carrier concentration due to thermal, free carrier, and optical
        %effects.
        
        dDNdt = -G_fc*DN + Nconst*Nfreq .* sq(U).^2;
    end
    %
    %
    %
    function [dDdt] = nl_diff_instant_opt_step(t, D)
        %NL_DIFF_INSTANT_OPT_STEP Evaluates the differential step for the
        %temperature variation and the free charge carriers density variation,
        %while the optical field is evaluated instantly.

        % Interpolates field E with t
%         input_fields = nan(1, nInputs);
%         for i = 1:nInputs
%             input_fields(i) = power*interp1( time, waves(i,:), t );
%         end

        % Unpack the input array, elems
        oldDT = D(1:nActive);
        oldDN = D(nActive+1:2*nActive);
        U = zeros( nActive, 1 );

        % Evaluate the variation of the refractive index due to nonlinearities
        dn = resonance_detuning( oldDT, oldDN, U );

        % Evaluate new optical field
        F = optical_fields( sqpower.*waves(t), dn );
        
        % Field to sqrt(energy) conversion
        U = field_to_energy .* F;

        % Evaluate temperature variation
        dDTdt = d_temperature(oldDT, oldDN, U);

        % Evaluate free carrier variation
        dDNdt = d_freecarriers(oldDN, U);

        % combine all the variations
        dDdt = [dDTdt; dDNdt];
    end

    ode_step = @nl_diff_instant_opt_step;
    if nargout > 1
        opt_fields = @(in, dn) par_field_eval( ...
            c0, fp, sqpower.*in, k, Length, neff, dn );
        res_dn = @resonance_detuning;
    end
end
