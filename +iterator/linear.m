function fun = linear(loader, parfun, frequency, nPorts, nActive)

    function [freq, opt] = step( j )
        
        c0 = 299.792458;       % [um/ps] c_0, speed of light in vacuum
        L = length(frequency);
        
        args = feval( loader, j);
        
        % power to field intensity
        sqpower = sqrt( args{1} );
        % refractive index imaginary part: losses
        PLoss   = (args{6} ./ 10 ) .* log( 10 ); % log( 10.^(dBLoss/10));
        f0 = c0/1.550;         % [THz] defining f0 for group index approximation in neff
        % Group index approximation of the effective index
        neff = @(f) f0/f .* ( args{5} + args{8} .* ( f - f0 ) ./ f0 ) ...
            + 1i*PLoss/2 * c0/(2*pi*f);

        linear_spectrum = @(fp) parfun( ...
            c0, fp, sqpower ./ max(sqpower), args{3:4}, ...
            neff(fp), zeros(nActive, 1) ...
        );
    
        opt = nan( nPorts, L);
        for j = 1:L
            opt(:, j) = linear_spectrum( frequency(j) );
        end
        freq = frequency;
    end
    
    fun = @step;
end