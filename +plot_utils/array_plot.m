classdef array_plot < handle
    %ARRAY_PLOT Plot a series of plots in array distribution.
    %   The default plot function is precise.plot_utils.contourf_robust,
    %   but can be easily changed.
    
    properties
        Title
        Figure
        Plots = struct( 'ax', [], 'xline', [], 'yline', [] )
        PlotFcn function_handle
        ReducedDim (1,1) double
        
        Data double
        Vectors table
        J2ind function_handle
    end
    
    methods
        function obj = array_plot(data, source_table, j2ind, rows, args)
            %ARRAY_PLOT Construct an instance of this class
            %   Detailed explanation goes here
            arguments
                data double
                source_table table
                j2ind function_handle
                rows (:,1) double { mustBeInteger, mustBePositive }
                args.Subtitles (:,1) cell = {}
                args.Title (1,:) char = ''
                args.PlotFcn function_handle = @precise.plot_utils.contourf_robust
                args.ReducedDim (1,1) double { mustBeInteger, ...
                    mustBeNonnegative } = 0
            end
            

            obj.J2ind = j2ind;
            obj.PlotFcn = args.PlotFcn;
            obj.ReducedDim = args.ReducedDim;
            
            Ch = size( data, 1 );
            
            if isscalar(rows)
                C = ceil( Ch / rows );
                R = rem( Ch, rows );
                rows = repelem( C, rows );
                if R
                    rows(end) = R;
                end
            elseif sum( rows ) ~= Ch
                error( 'Wrong number of axes listed in rows.' ),
            end
            
            if isempty( args.Subtitles )
                args.Subtitles = cell( sum(rows), 1 );
            elseif length( args.Subtitles ) == length( rows )
                j = 0;
                tmp = cell( sum(rows), 1 );
                for r = 1:length(rows)
                    n = j + (1:rows(r));
                    tmp( n ) = args.Subtitles( r );
                    j = n( end );
                end
                args.Subtitles = tmp;
            elseif numel( args.Subtitles ) ~= sum( rows )
                error( 'Wrong number sub-titles listed.' ),
            end
            
            dims = cellfun( @length, source_table.value )';
            
            obj.Data = permute( ...
                reshape( data, [Ch, dims]), ...
                [2, 3, 1, 4:length(dims)+1] ...
            );
        
            obj.Vectors = addvars( source_table, ...
                num2cell( ones( height(source_table), 1 ) ), ...
                'NewVariableNames', "current" );
            
            % Create new figure
            obj.Figure = figure( 'Name', args.Title );
            
            % Draw axes
            obj.draw_axes( rows, args.Subtitles );
            
            % Print subplot grid titles
            obj.Title = sgtitle( args.Title );
            
            % Update plot
            obj.update_plots()
        end
        
        function select_parameters( obj, x, y )
            arguments
                obj
                x (1,1) double { mustBeInteger, mustBePositive }
                y (1,1) double { mustBeInteger, mustBePositive }
            end
            [~, id] = sort( obj.Vectors.id );
            id = unique( [id(x); id(y); id]', 'stable');
            
            obj.Vectors = obj.Vectors( id, : );
            
            id( id > 2 ) = id( id > 2 ) + 1;
            
            obj.Data = permute( obj.Data, [id(1:2), 3, id(3:end)]);
            
            obj.update_plots( );
        end
        
        function draw_axes( obj, rows, Subtitles )
            id = 1;
            L = length(rows);
            r = 0;
            for j=1:L
                C = rows(j);
                for k=1:C
                    r = r + 1;
                    obj.Plots(id) = struct( ...
                        'ax', subplot( L, C, k + (j-1)*C ), ...
                        'xline', xline( 0, ':r', 'LineWidth', 2 ), ...
                        'yline', yline( 0, ':r', 'LineWidth', 2 ) ...
                    );
                    title( Subtitles( r ) );
                    id = id+1;
                end
            end
        end
        
        function update_plots( obj )
            figure( obj.Figure );
            
            xval = obj.Vectors.value{2}( obj.Vectors.current{2} );
            yval = obj.Vectors.value{1}( obj.Vectors.current{1} );
            
            for j=1:length( obj.Plots )
                subplot( obj.Plots( j ).ax );
                set(gca,'NextPlot','replacechildren');
                % plot data
                obj.PlotFcn( obj.Vectors.value{2}, obj.Vectors.value{1}, ...
                    obj.Data(:,:,j, obj.Vectors.current{3:end} ) );
                axis tight;
                % plot crosshair
                obj.Plots( j ).xline = xline( xval, ':r', 'LineWidth', 2 );
                obj.Plots( j ).yline = yline( yval, ':r', 'LineWidth', 2 );
                % redefine labels
                xlabel( obj.Vectors.label{2} )
                ylabel( obj.Vectors.label{1} )
                % plot colorbar
                colorbar
            end
        end
        
        function update_crosshair( obj, idx, idy )
            figure( obj.Figure );
            for j=1:length( obj.Plots )
                obj.Plots(j).xline.Value = obj.Vectors.value{2}( idx );
                obj.Plots(j).yline.Value = obj.Vectors.value{1}( idy );
            end
        end
        
        function update( obj, j )
            if isnumeric( j ) && isscalar( j )
                indexes = obj.J2ind( j );
            elseif iscell( j )
                if obj.ReducedDim
                    j( obj.ReducedDim ) = [];
                end
                indexes = j;
            else
                error( 'input must be either global index j or sub-indexes cell array.' )
            end
            indexes = indexes( obj.Vectors.id )';
            tf = all( cellfun(@isequal, indexes(3:end), ...
                obj.Vectors.current(3:end)) );
            obj.Vectors.current = indexes;
            if tf
                obj.update_crosshair( indexes{ [2, 1] } );
            else
                obj.update_plots();
            end
        end
    end
end

