function out = imagesc_nonuniform(x, y, data)
%IMAGESC_NONUNIFORM Plot data values on a ununiform grid.
%   This function plots a 2D array of pixel, one for each point in
%   the data matrix. The pixel can be non uniformly spaced.
%
%   This function duplicates the data on the last row and column, obtaining
%   an array of (M+1)x(N+1) elements. Then proceeds to plot all values in
%   MxN pixels employing the low-level function pcolor.
    arguments
        x (1,:) double      % 1D x axis vector
        y (1,:) double      % 1D y axis vector
        data (:,:) double   % 2D data matrix
    end
    
    if length(x) > 1
        x = movmean( [2*x(1) - x(2), x, 2*x(end) - x(end-1) ], [0, 1], ...
            "Endpoints", 'discard' );
    else
        x = x .* [0.99, 1.01];
    end
    if length(y) > 1
        y = movmean( [2*y(1) - y(2), y, 2*y(end) - y(end-1) ], [0, 1], ...
            "Endpoints", 'discard' );
    else
        y = y .* [0.99, 1.01];
    end
    
    z = [ data, data(:, end); data(end, :), data(end,end) ];
    
    if nargout
        out = pcolor( x, y, z );
    else
        pcolor( x, y, z );
    end
end

