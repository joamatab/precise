function p = plot_bifurcation_colored( x, y, z, ax, normalize, args, ...
    propArgs )
    arguments
        x (1,:) double
        y (1,:) cell
        z (1,:) cell = {}
        ax matlab.graphics.axis.Axes = gca;
        normalize = true
        args.MarkerSize = 36
        args.cmap = 'jet'
        propArgs.?matlab.graphics.chart.primitive.Scatter
    end
    propertyCell = namedargs2cell(propArgs);
    
    tf = isempty( z );
    
    N = length( x );
    L = cellfun( @length, y);
    l = sum( L );
    
    X = nan( l, 1);
    Y = nan( l, 1);
    if ~tf
        Z = nan( l, 1);
    end
    
    k = 0;
    for i=1:N
        ii = k + ( 1:L(i) );
        
        X( ii ) = x(i);
        Y( ii ) = y{i};
        if ~tf
            if normalize
                Z( ii ) = z{i}/sum(z{i});
            else
                Z( ii ) = z{i};
            end
        end
        
        k = ii(end);
    end
    
    if tf
        p = scatter( ax, X, Y, args.MarkerSize, 'filled', propertyCell{:});
    else
        p = scatter( ax, X, Y, args.MarkerSize, Z, 'filled', propertyCell{:});
        colormap( ax, args.cmap );
        caxis( ax, [0 1] );
        colorbar( ax );
    end
    
end