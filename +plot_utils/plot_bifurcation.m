function plot_bifurcation( x, y, LineSpec )
    arguments
        x (1,:) double
        y (:,:) cell
        LineSpec = '+'
    end
    
    [N, M] = size( y );
    L = cellfun( @length, y);
    l = sum( L, 2 );
    
    for i=1:N
        clear XL YL
        XL = nan( 1, l(i) );
        YL = nan( 1, l(i) );
        
        id = 1;
        for j=1:M
            k = L(i,j);
            
            XL( id:id+k-1 ) = x( repmat(j, [1,k] ) );
            YL( id:id+k-1 ) = y{i,j};
            id = id + k;
        end
        
        plot( XL, YL, LineSpec);
        hold on;
    end
    hold off;
    
%     for i=1:N
%         clear L XL YL
%         XL = nan( 1, l(i) );
%         YL = nan( 1, l(i) );
%         
%         id = 1;
%         for j=1:M
% 
%             XL( id:id+l-1 ) = repmat(j, [1,L(i,j)] );
%             YL( id:id+l-1 ) = y{i,j};
%             id = id + l+1;
%         end
%         
%         plot( XL, YL, '.');
%     end
    
end