function out = contourf_robust( x, y, z)
%CONTOURF_ROBUST Wrapper for the contourf plot function. This function
%allows to plot even 1x1, 1xN, or Mx1 data matrices
    arguments
        x (1,:) double % 1D x axis vector
        y (1,:) double % 1D y axis vector
        z (:,:) double % 2D data matrix
    end
    
    if length(x) == 1
        x = x .* [0.99, 1.01];
        z = [z, z];
    end
    if length(y) == 1
        y = y .* [0.99, 1.01];
        z = [z; z];
    end
    
    
    if nargout
        out = contourf( x, y, z );
    else
        contourf( x, y, z );
    end
end