classdef extremal < precise.plot_utils.array_plot
    %EXTREMAL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        DataCellValues
        BifPlot
    end
    
    methods
        function obj = extremal(values, source_table, j2ind, rows, args )
            %EXTREMAL Construct an instance of this class
            %   Detailed explanation goes here
            arguments
                values (:,:) cell
                source_table table
                j2ind function_handle
                rows (:,1) double { mustBeInteger, mustBePositive }
                args.Subtitles (:,1) cell = {}
                args.Title (1,:) char = ''
            end
            
            cargs = namedargs2cell( args );
            
            Data = cellfun( @length, values );
            
            obj@precise.plot_utils.array_plot( Data, source_table, ...
                j2ind, rows, cargs{:}, ...
                'PlotFcn', @precise.plot_utils.imagesc_nonuniform, ...
                'ReducedDim', false ...
            );
            
            dims = cellfun( @length, source_table.value )';
            
            obj.DataCellValues = permute( ...
                reshape( values, [size(values,1), dims]), ...
                [2, 3, 1, 4:length(dims)+1] ...
            );
            
            obj.draw_floating_axis( args.Subtitles )
        end
        
        function select_parameters( obj, x, y)
            [~, id] = sort( obj.Vectors.id );
            id = unique( [id(x); id(y); id]', 'stable');
            id( id > 2 ) = id( id > 2 ) + 1;
            
            obj.DataCellValues = permute( obj.DataCellValues, ...
                [id(1:2), 3, id(3:end)]);
            
            select_parameters@precise.plot_utils.array_plot( obj, x, y )
            obj.update_bifurcation_diagram( obj.Vectors.current{1} );

        end
        
        function draw_floating_axis( obj, Subtitles )
            obj.BifPlot.f = figure( 'Name', 'Bifurcation Diagram' );

            obj.BifPlot.tg = uitabgroup( obj.BifPlot.f );

            for ii = 1:size( obj.DataCellValues, 3 )
              obj.BifPlot.tab(ii) = uitab( obj.BifPlot.tg, ...
                  'Title', Subtitles{ ii } );
              obj.BifPlot.ax(ii) = axes('Parent', obj.BifPlot.tab(ii) );
            end
            
            obj.update_bifurcation_diagram( obj.Vectors.current{1} )
            title( 'Bifurcation Diagram' );
        end
        
        function update_bifurcation_diagram( obj, i )
            
            for ii = 1:size( obj.DataCellValues, 3 )
                figure( obj.BifPlot.f );
                ids = obj.Vectors.current{3:end};
                rowDataY = squeeze( obj.DataCellValues( i, :, ii, ids ) );

                ax = obj.BifPlot.ax( ii );
                cla( ax );

                precise.plot_utils.plot_bifurcation_colored( ...
                    obj.Vectors.value{2}, rowDataY, {}, ax );
                xlabel( ax, obj.Vectors.label{2} );

                xval = obj.Vectors.value{2}( obj.Vectors.current{2} );
                obj.BifPlot.xline(ii) = xline( ax, xval, ...
                    ':r', 'LineWidth', 2 );
            end
        end

        function update_crosshair( obj, idx, idy )
            update_crosshair@precise.plot_utils.array_plot( obj, idx, idy );
            for j = 1:length( obj.BifPlot.xline )
                obj.BifPlot.xline(j).Value = obj.Vectors.value{2}( idx );
            end
        end

        function update( obj, j )
            if isnumeric( j ) && isscalar( j )
                indexes = obj.J2ind( j );
            elseif iscell( j ) && length( j ) == height( obj.Vectors )
                indexes = j;
            else
                error( 'input must be either global index j or sub-indexes cell array.' )
            end
            indexes = indexes( obj.Vectors.id )';
            tf = all( cellfun(@isequal, indexes(3:end), ...
                obj.Vectors.current(3:end)) );
            bif = indexes{1} == obj.Vectors.current{1};
            obj.Vectors.current = indexes;
            if tf && bif
                if ~bif
                    obj.update_bifurcation_diagram( indexes{1} )
                end
                obj.update_crosshair( indexes{ [2, 1] } );
            else
                obj.update_bifurcation_diagram( indexes{1} )
                obj.update_plots();
            end
        end
    end
end
